# N queens Implementation

Repostory structure:

* generate: Python scripts for VHDL modules generation
* nqueens_udma: Implementation example of the UDMA scripts
* jupyter: Jupyter notebook with an example implementation using IPython Parallel backend on a cluster of FPGAs.

Note: UDMA refers to the global interface library [UDMA Mlab](https://gitlab.com/ictp-mlab/udma).
