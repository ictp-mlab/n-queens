library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity demux_presol is
    port(
        clk, nRst: in std_logic;
        read_0,read_1,read_2,read_3,read_4,read_5,read_6,read_7,read_8,read_9,read_10,read_11,read_12,read_13,read_14,read_15: in std_logic;
        data_I : in std_logic_vector(31 downto 0);
        empt_I : in std_logic;
        data_0,data_1,data_2,data_3,data_4,data_5,data_6,data_7,data_8,data_9,data_10,data_11,data_12,data_13,data_14,data_15: out std_logic_vector(31 downto 0);
        empty_0,empty_1,empty_2,empty_3,empty_4,empty_5,empty_6,empty_7,empty_8,empty_9,empty_10,empty_11,empty_12,empty_13,empty_14,empty_15: out std_logic;
        r_fifo: out std_logic
 );
end demux_presol;
architecture Behavioral of demux_presol is
    signal S: unsigned(4 downto 0);
    signal r_s: std_logic;
begin

    
    data_0 <= data_I;

    data_1 <= data_I;

    data_2 <= data_I;

    data_3 <= data_I;

    data_4 <= data_I;

    data_5 <= data_I;

    data_6 <= data_I;

    data_7 <= data_I;

    data_8 <= data_I;

    data_9 <= data_I;

    data_10 <= data_I;

    data_11 <= data_I;

    data_12 <= data_I;

    data_13 <= data_I;

    data_14 <= data_I;

    data_15 <= data_I;
    r_fifo <= r_s;
    r_s <= read_0 or read_1 or read_2 or read_3 or read_4 or read_5 or read_6 or read_7 or read_8 or read_9 or read_10 or read_11 or read_12 or read_13 or read_14 or read_15;


    with S select
        empty_0 <= empt_I when "00000",
            '1' when others;

    with S select
        empty_1 <= empt_I when "00001",
            '1' when others;

    with S select
        empty_2 <= empt_I when "00010",
            '1' when others;

    with S select
        empty_3 <= empt_I when "00011",
            '1' when others;

    with S select
        empty_4 <= empt_I when "00100",
            '1' when others;

    with S select
        empty_5 <= empt_I when "00101",
            '1' when others;

    with S select
        empty_6 <= empt_I when "00110",
            '1' when others;

    with S select
        empty_7 <= empt_I when "00111",
            '1' when others;

    with S select
        empty_8 <= empt_I when "01000",
            '1' when others;

    with S select
        empty_9 <= empt_I when "01001",
            '1' when others;

    with S select
        empty_10 <= empt_I when "01010",
            '1' when others;

    with S select
        empty_11 <= empt_I when "01011",
            '1' when others;

    with S select
        empty_12 <= empt_I when "01100",
            '1' when others;

    with S select
        empty_13 <= empt_I when "01101",
            '1' when others;

    with S select
        empty_14 <= empt_I when "01110",
            '1' when others;

    with S select
        empty_15 <= empt_I when "01111",
            '1' when others;

    process (clk) is
    begin
        if rising_edge(clk) then
            if nRst = '1' then 
                S <= (others => '0');
            elsif (r_s = '1') then
                if (S = "01111") then
                    S <= (others => '0');
                else
                    S <= S + 1;
                end if;
            end if;
        end if;
    end process;
    
end Behavioral;
