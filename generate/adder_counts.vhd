library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity adder_counts is
 port(
     clk_i: in std_logic;
     nRst: in std_logic;
     count_0,count_1,count_2,count_3,count_4,count_5,count_6,count_7,count_8,count_9,count_10,count_11,count_12,count_13,count_14,count_15: in std_logic_vector(63 downto 0);
     done_0,done_1,done_2,done_3,done_4,done_5,done_6,done_7,done_8,done_9,done_10,done_11,done_12,done_13,done_14,done_15: in std_logic;
     coun_O : out std_logic_vector(63 downto 0);
     done_O : out std_logic
 );
end adder_counts;
architecture Behavioral of adder_counts is
    signal cs_0,cs_1,cs_2,cs_3,cs_4,cs_5,cs_6,cs_7,cs_8,cs_9,cs_10,cs_11,cs_12,cs_13,cs_14,cs_15: unsigned(63 downto 0);
    signal s_count: unsigned(63 downto 0);
    signal s_done: std_logic;
begin

    cs_0 <= unsigned(count_0);
    cs_1 <= unsigned(count_1);
    cs_2 <= unsigned(count_2);
    cs_3 <= unsigned(count_3);
    cs_4 <= unsigned(count_4);
    cs_5 <= unsigned(count_5);
    cs_6 <= unsigned(count_6);
    cs_7 <= unsigned(count_7);
    cs_8 <= unsigned(count_8);
    cs_9 <= unsigned(count_9);
    cs_10 <= unsigned(count_10);
    cs_11 <= unsigned(count_11);
    cs_12 <= unsigned(count_12);
    cs_13 <= unsigned(count_13);
    cs_14 <= unsigned(count_14);
    cs_15 <= unsigned(count_15);
    coun_O <= std_logic_vector(s_count);
    done_O <= s_done;

    process (clk_i)
    begin
        if rising_edge(clk_i)  then
            if nRst = '1' then
                s_done <= '0';
                s_count <= (others => '0');
            else
                s_count <= (cs_0 + cs_1 + cs_2 + cs_3 + cs_4 + cs_5 + cs_6 + cs_7 + cs_8 + cs_9 + cs_10 + cs_11 + cs_12 + cs_13 + cs_14 + cs_15);
                s_done <= done_0 and done_1 and done_2 and done_3 and done_4 and done_5 and done_6 and done_7 and done_8 and done_9 and done_10 and done_11 and done_12 and done_13 and done_14 and done_15;
            end if;
        end if;
    end process;
end Behavioral;
